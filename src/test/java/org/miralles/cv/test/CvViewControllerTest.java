package org.miralles.cv.test;

import junit.framework.TestCase;

import org.junit.Test;
import org.mmiralles.controller.CvViewController;
import org.springframework.ui.ModelMap;

public class CvViewControllerTest extends TestCase
{

	@Test
    public void testNewRequestEnglishDocument() throws Exception
    {
        CvViewController controller = new CvViewController();
        ModelMap model = new ModelMap();
        String modelAndView = controller.printEnglishCv(model, null);
        assertEquals("This should be not null and equals cv.jsp", "cv.jsp", modelAndView);
    }

    @Test
    public void testValidRequestEnglishDocument() throws Exception
    {
        CvViewController controller = new CvViewController();
        ModelMap model = new ModelMap();
        controller.printEnglishCv(model, null);
        assertEquals("This should be not null and equals Mauricio", "Mauricio",
                model.get("cv_en_name"));
    }
}