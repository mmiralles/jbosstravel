package org.miralles.cv.test;

import static org.junit.Assert.*;

import java.net.UnknownHostException;
import java.text.ParseException;

import org.junit.Test;
import org.mmiralles.bo.Cv;
import org.mmiralles.services.CvService;
import org.mmiralles.services.impl.CvServiceImp;

public class JsonAdapterTest
{

	private static final String LOCALHOST = "localhost";
	private static final String DOCUMENT_EN = "cv_en";
	private static final String SOME_LANGUAGE = "Spanish";
	private static final String COLLEGE_DESCRIPTION = "Technical engineering software development (1997-2000)";

	private Cv beginScenario() throws UnknownHostException, ParseException {
		CvService cvService = new CvServiceImp();
		
		Cv cvDocument = cvService.getCvByName(DOCUMENT_EN, LOCALHOST);
		return cvDocument;
	}
	
    @Test
    public void parseNotNullMongoDocument() throws UnknownHostException, ParseException
    {

        Cv cvDocument = beginScenario();

        assertNotNull("This should not be null", cvDocument);
    }
    
    @Test
    public void retrieveDegree() throws UnknownHostException, ParseException{
    	
    	Cv cvDocument = beginScenario();
        
        assertEquals("Should be Technical engineering software development (1997-2000)",COLLEGE_DESCRIPTION, cvDocument.getCollege());
    }
    
    @Test
    public void retrieveLanguages() throws UnknownHostException, ParseException{
    	Cv cvDocument = beginScenario();
    	
    	assertEquals("Should be Spanish",SOME_LANGUAGE, cvDocument.retrieveLanguages().get(0).getTitle());
    }
}