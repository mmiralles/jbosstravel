package org.miralles.cv.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mmiralles.cv.controller.MongoDbProvider;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

public class MongodbTest
{

    private static final String COLLEGE = "college";
	private static final String DEGREES = "degrees";
	private static final String CONTENT = "content";
	private static final String COLLEGE_TITLE = "Technical engineering software development (1997-2000)";
	private static final String QUERY_PARAMETER = "cv_en";
    private static final String DOCUMENT_KEY = "cv_name";
    private static final String COLLECTION_NAME = "cvonline";
    private static final String CVONLINE_COLLECTION = COLLECTION_NAME;
    private static final String LOCALHOST = "localhost";
    private MongoDbProvider mongodbProvider;
    private static final String ANY_COMPANY = "Rumbo";

    private Integer createScenario() throws UnknownHostException
    {
        mongodbProvider = new MongoDbProvider();
        return mongodbProvider.connect(LOCALHOST);
    }

    private String retrieveCompanyName() {
    	BasicDBObject cvDocument = mongodbProvider.retrieveDocumentByName(COLLECTION_NAME,
    			QUERY_PARAMETER);
    	BasicDBObject content = (BasicDBObject) cvDocument.get(CONTENT);        
    	BasicDBObject experience = (BasicDBObject) content.get("experience");
    	BasicDBObject rumbo = (BasicDBObject) experience.get("1");
    	String clientName = rumbo.getString("client");
    	return clientName;
    }

    @Test
    public void mongoDbIsAlive() throws UnknownHostException
    {
        Integer conected = createScenario();

        assertNotNull("This should not be null", conected);
    }

    @Test
    public void hasMongodbCollections() throws UnknownHostException
    {
        createScenario();

        Set<String> collections = mongodbProvider.retrieveCollectionNames();

        assertNotNull("This should not be null", collections);
    }

    @Test
    public void hasMongodbCvOnlineCollection() throws UnknownHostException
    {
        createScenario();

        Set<String> collections = mongodbProvider.retrieveCollectionNames();

        assertTrue("Should be cvonline", collections.contains(CVONLINE_COLLECTION));
    }

    @Test
    public void retrieveNotNullCollection() throws UnknownHostException
    {
        createScenario();

        DBCollection coll = mongodbProvider.retrieveCollectionByName(COLLECTION_NAME);

        assertNotNull("Should be not null", coll);
    }

    @Test
    public void retrieveNotNullDocument() throws UnknownHostException
    {
        createScenario();

        BasicDBObject cvDocument = mongodbProvider.retrieveDocumentByName(COLLECTION_NAME,
                QUERY_PARAMETER);

        assertNotNull("Should be not null", cvDocument);
    }

    @Test
    public void retrieveValidDocument() throws UnknownHostException
    {
        createScenario();

        BasicDBObject cvDocument = mongodbProvider.retrieveDocumentByName(COLLECTION_NAME,
                QUERY_PARAMETER);

        assertEquals("Should be cv_en", QUERY_PARAMETER, cvDocument.get(DOCUMENT_KEY));
    }
    
    @Test
    public void retrieveValidExperienceValue() throws UnknownHostException
    {
        createScenario();

        String clientName = retrieveCompanyName();

        assertEquals("Should be Rumbo", ANY_COMPANY, clientName);
    }
    
    @Test
    public void retrieveDegrees() throws UnknownHostException{
    	createScenario();
    	
    	BasicDBObject cvDocument = mongodbProvider.retrieveDocumentByName(COLLECTION_NAME,
                QUERY_PARAMETER);
    	
    	BasicDBObject college = retrieveCollege(cvDocument);
    	
    	assertEquals("Should be Technical engineering software development (1997-2000)",COLLEGE_TITLE, college.getString("description"));
    	
    }

    @Test
    public void retrieveLanguages() throws UnknownHostException{
    	createScenario();
    	
    	BasicDBObject language = retrieveSpanishLanguage();
    	
    	assertEquals("Should be Spanish","Spanish", language.get("title"));
    }

	private BasicDBObject retrieveSpanishLanguage() {
		BasicDBObject language=null;
		BasicDBObject cvDocument = mongodbProvider.retrieveDocumentByName(COLLECTION_NAME,
    			QUERY_PARAMETER);
    	BasicDBObject cvEn = (BasicDBObject) cvDocument.get(CONTENT);
    	BasicDBObject languageNode = (BasicDBObject) cvEn.get("languages");
    	Collection<Object> languageCol = languageNode.values();
		for (Object object : languageCol) {
			language = (BasicDBObject)object;
			if(language.get("title").equals("Spanish")){
				return language;
			}
		}
		return language;
	}
    
	private BasicDBObject retrieveCollege(BasicDBObject cvDocument) {
		BasicDBObject cvEn = (BasicDBObject) cvDocument.get(CONTENT);
    	BasicDBObject degrees = (BasicDBObject) cvEn.get(DEGREES);
        BasicDBObject college = (BasicDBObject) degrees.get(COLLEGE);
		return college;
	}
    
}