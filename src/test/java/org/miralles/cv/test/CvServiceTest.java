package org.miralles.cv.test;

import static org.junit.Assert.assertNotNull;

import java.net.UnknownHostException;
import java.text.ParseException;

import org.junit.Test;
import org.mmiralles.bo.Cv;
import org.mmiralles.services.CvService;
import org.mmiralles.services.impl.CvServiceImp;

public class CvServiceTest
{

    private static final String LOCALHOST = "localhost";
	private static final String CV_EN = "cv_en";

	@Test
    public void retrieveDocumentByName() throws UnknownHostException, ParseException
    {
        CvService cvService = new CvServiceImp();

        Cv cvDocument = cvService.getCvByName(CV_EN, LOCALHOST);

        assertNotNull("This should not be null", cvDocument);
    }

}
