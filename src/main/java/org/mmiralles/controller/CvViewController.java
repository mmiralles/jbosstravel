package org.mmiralles.controller;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.mmiralles.bo.Cv;
import org.mmiralles.services.CvService;
import org.mmiralles.services.impl.CvServiceImp;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
public class CvViewController
{

    private static final String CV_EN = "cv_en";
    private static final String RETURN_VIEW_CV = "cv.jsp";
    private static final String MONGO_URL = "127.10.225.2";

    @RequestMapping("/cv_en.htm")
    public String printEnglishCv(ModelMap model, HttpServletRequest request) throws IOException, JSONException,
            ParseException
    {
        CvService cvService = new CvServiceImp();

        if(request!=null){
        	System.out.println(request.getLocalName());
        	
        }
        String url = "localhost";
        if(null!=request && !"localhost".equals(request.getLocalName())){
        	url = MONGO_URL;
        }

        Cv cvEnglish = cvService.getCvByName(CV_EN, url);
        fillModel(model, cvEnglish);

        return RETURN_VIEW_CV;
    }

    private void fillModel(ModelMap model,
                           Cv cvEnglish)
    {
        model.addAttribute("cv_en_name", cvEnglish.getName());
        model.addAttribute("cv_en_surname", cvEnglish.getSurname());
        model.addAttribute("cv_en_id", cvEnglish.getId());
        model.addAttribute("cv_en_birddate", cvEnglish.getBirdDate().toString());
        model.addAttribute("cv_en_skype", cvEnglish.getSkype());
        model.addAttribute("cv_en_email", cvEnglish.getEmail());

        model.addAttribute("cv_en_mongourl", cvEnglish.getMongoUrl());
        model.addAttribute("cv_en_mongoimg", cvEnglish.getMongoImg());
        
        model.addAttribute("cv_en_college", cvEnglish.getCollege());

        model.addAttribute("cv_en_companies", cvEnglish.retrieveCompanies());
        
        model.addAttribute("cv_en_languages", cvEnglish.retrieveLanguages());
    }
}