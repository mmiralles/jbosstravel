package org.mmiralles.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;

import org.json.JSONException;
import org.mmiralles.bo.Company;
import org.mmiralles.bo.Cv;
import org.mmiralles.bo.Language;

import com.mongodb.BasicDBObject;

public class JsonAdapter
{

    private BasicDBObject cvDocument;
    private Cv result;

    public JsonAdapter(BasicDBObject pCvDocument)
    {
        cvDocument = pCvDocument;
    }

    public Cv parseMongoDocument() throws ParseException
    {
        result = new Cv();

        fillCv();

        return result;
    }

    private void fillCv() throws ParseException
    {
        BasicDBObject cvContent = (BasicDBObject) cvDocument.get("content");

        fillMongoPersonalData(cvContent);
        fillMongoDegree(cvContent);
        fillMongoExperience(cvContent);
        fillMongoLanguages(cvContent);
    }

    private void fillMongoLanguages(BasicDBObject jsonCvEn) {
    	BasicDBObject language = (BasicDBObject) jsonCvEn.get("languages");
        Set<String> languagesSetName = language.keySet();
        Object[] languagesName = (Object[]) languagesSetName.toArray();
        Arrays.sort(languagesName);

        for (Object languageName : languagesName)
        {
            result.add(fillMongoLanguage(language, "" + languageName));
        }		
	}

	private Language fillMongoLanguage(BasicDBObject language, String languageName) {
		
		BasicDBObject languageFinal = (BasicDBObject) language.get(languageName);
        Language languageResult = new Language();
        
        languageResult.setTitle(languageFinal.getString("title"));
        languageResult.setLevel(languageFinal.getString("level"));
        
        return languageResult;
	}

	private void fillMongoPersonalData(BasicDBObject jsonCvEn) throws JSONException,
            ParseException
    {
        String firstName = (String) jsonCvEn.get("name");
        result.setName(firstName);

        String surName = (String) jsonCvEn.get("surname");
        result.setSurname(surName);

        String id = (String) jsonCvEn.get("id");
        result.setId(id);

        Date birdDate = new SimpleDateFormat("dd/MM/yyyy").parse("" + jsonCvEn.get("date_bird"));
        result.setBirdDate(birdDate);

        String skype = (String) jsonCvEn.get("skype");
        result.setSkype(skype);

        String email = (String) jsonCvEn.get("email");
        result.setEmail(email);
    }

    private void fillMongoDegree(BasicDBObject jsonCvEn)
    {
        BasicDBObject degrees = (BasicDBObject) jsonCvEn.get("degrees");
        BasicDBObject mongoDegre = (BasicDBObject) degrees.get("mongodb");
        BasicDBObject college = (BasicDBObject) degrees.get("college");

        result.setMongoUrl(mongoDegre.getString("mongoURL"));
        result.setMongoImg(mongoDegre.getString("mongoImg"));
        
        result.setCollege(college.getString("description"));
    }

    private void fillMongoExperience(BasicDBObject jsonCvEn)
    {
        BasicDBObject experience = (BasicDBObject) jsonCvEn.get("experience");
        Set<String> companiesSetName = experience.keySet();
        Object[] companiesName = (Object[]) companiesSetName.toArray();
        Arrays.sort(companiesName);

        for (Object companyName : companiesName)
        {
            result.add(fillMongoCompany(experience, "" + companyName));
        }
    }

    private Company fillMongoCompany(BasicDBObject experience,
                                     String companyName)
    {
        BasicDBObject company = (BasicDBObject) experience.get(companyName);
        Company companyResult = new Company();
        companyResult.setClient(company.getString("client"));
        companyResult.setDuration(company.getString("duration"));
        companyResult.setCusiosities(company.getString("curiosities"));
        companyResult.setName(company.getString("name"));
        companyResult.setTecnologies(company.getString("tecnologies"));
        companyResult.setRole(company.getString("role"));
        return companyResult;
    }
}
