package org.mmiralles.cv.controller;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Set;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class MongoDbProvider
{

    private static final String QUERY_FILTER = "cv_name";
    private static final String DATABASE = "jbosstravel";
    private DB db;

    public Integer connect(String url) throws UnknownHostException
    {
        Integer toReturn = null;
        MongoClient mongoClient;

        mongoClient = new MongoClient(url, 27017);
        
        db = mongoClient.getDB("jbosstravel");
        if (!db.authenticate("cvonline", "password".toCharArray())) {
        	//TODO:Manage this case
        }
        
        db = mongoClient.getDB(DATABASE);
        if (null != db)
            toReturn = 1;

        return toReturn;
    }

    public Set<String> retrieveCollectionNames()
    {
        Set<String> colls = db.getCollectionNames();

        return colls;
    }

    public DBCollection retrieveCollectionByName(String collectionName)
    {
        DBCollection coll = db.getCollection(collectionName);

        return coll;
    }

    public BasicDBObject retrieveDocumentByName(String collectionName,
                                                String name)
    {
        BasicDBObject query = new BasicDBObject(QUERY_FILTER, name);

        DBCollection coll = retrieveCollectionByName(collectionName);
        DBCursor lstCvonline = coll.find(query);
        BasicDBObject cvonline = (BasicDBObject) lstCvonline.next();

        return cvonline;
    }
}
