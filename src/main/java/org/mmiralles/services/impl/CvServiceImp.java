package org.mmiralles.services.impl;

import java.net.UnknownHostException;
import java.text.ParseException;

import org.mmiralles.adapters.JsonAdapter;
import org.mmiralles.bo.Cv;
import org.mmiralles.cv.controller.MongoDbProvider;
import org.mmiralles.services.CvService;

import com.mongodb.BasicDBObject;

public class CvServiceImp implements CvService
{

    private static final String CV_COLLECTION = "cvonline";

	@Override
    public Cv getCvByName(String documentName, String mongoUrl) throws UnknownHostException, ParseException
    {

        MongoDbProvider mongoProvider = new MongoDbProvider();
        //mongoProvider.connect("localhost");// localhost
        //mongoProvider.connect("127.10.225.2");// 127.10.225.2
        mongoProvider.connect(mongoUrl);

        BasicDBObject cvDocument = mongoProvider.retrieveDocumentByName(CV_COLLECTION, documentName);

        JsonAdapter jsonAdapter = new JsonAdapter(cvDocument);
        Cv cv = jsonAdapter.parseMongoDocument();
        return cv;
    }

}
