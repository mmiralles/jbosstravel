package org.mmiralles.services;

import java.net.UnknownHostException;
import java.text.ParseException;

import org.mmiralles.bo.Cv;

public interface CvService
{
    Cv getCvByName(String documentName, String mongoUrl) throws UnknownHostException, ParseException;
}
