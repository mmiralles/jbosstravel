package org.mmiralles.bo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mmiralles.bo.Company;

public class Cv {
	private String name;
	private String surname;
	private Date birdDate;
	private String id;
	private String mongoUrl;
	private String mongoImg;
	private List<Company> companies;
	private String skype;
	private String email;
	private String college;
	private List<Language> languages;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMongoUrl() {
		return mongoUrl;
	}
	public void setMongoUrl(String mongoUrl) {
		this.mongoUrl = mongoUrl;
	}
	public String getMongoImg() {
		return mongoImg;
	}
	public void setMongoImg(String mongoImg) {
		this.mongoImg = mongoImg;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Date getBirdDate() {
		return birdDate;
	}
	public void setBirdDate(Date birdDate) {
		this.birdDate = birdDate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<Company> retrieveCompanies() {
		return companies;
	}
	public void add(Company company){
		if(companies==null)
			companies = new ArrayList<Company>();
		companies.add(company);
	}
	public List<Language> retrieveLanguages() {
		return languages;
	}
	public void add(Language language){
		if(languages==null)
			languages = new ArrayList<Language>();
		languages.add(language);
	}
	public String getSkype() {
		return skype;
	}
	public void setSkype(String skype) {
		this.skype = skype;
	}
	public String getCollege() {
		return college;
	}
	public void setCollege(String college) {
		this.college = college;
	}
}
