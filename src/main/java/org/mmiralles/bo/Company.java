package org.mmiralles.bo;

public class Company
{

    private String name;
    private String role;
    private String client;
    private String tecnologies;
    private String cusiosities;
    private String duration;

    public String getDuration() {
		return duration;
	}

	public String getRole()
    {
        return role;
    }

    public String getClient()
    {
        return client;
    }

    public String getTecnologies()
    {
        return tecnologies;
    }

    public String getCusiosities()
    {
        return cusiosities;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    public void setClient(String client)
    {
        this.client = client;
    }

    public void setTecnologies(String tecnologies)
    {
        this.tecnologies = tecnologies;
    }

    public void setCusiosities(String cusiosities)
    {
        this.cusiosities = cusiosities;
    }

    public String getName()
    {
        return name;
    }

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
