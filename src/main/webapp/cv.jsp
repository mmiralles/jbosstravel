<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="EN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description"
	content="A personal website that shows the 0 and 1s I know.">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>English Online C.V.</title>

<!-- Add to homescreen for Chrome on Android -->
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" sizes="192x192"
	href="images/touch/chrome-touch-icon-192x192.png">

<!-- Add to homescreen for Safari on iOS -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="NoSoloDeFotos">
<link rel="apple-touch-icon-precomposed"
	href="apple-touch-icon-precomposed.png">

<!-- Tile icon for Win8 (144x144 + tile color) -->
<meta name="msapplication-TileImage"
	content="images/touch/ms-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#3372DF">

<!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
<!--
    <link rel="canonical" href="http://www.nosolodefotos.com/">
    -->

<!-- Page styles -->
<link rel="stylesheet" href="styles/main.css">
</head>
<body>
	<header class="app-bar promote-layer">
		<div class="app-bar-container">
			<button class="menu">
				<img src="images/hamburger.svg" alt="Menu">
			</button>
			<h1 class="logo">
				<strong>English C.V.</strong>
			</h1>
			<section class="app-bar-actions">
				<!-- Put App Bar Buttons Here -->
				<!-- e.g <button><i class="icon icon-star"></i></button> -->
			</section>
		</div>
	</header>

	<nav class="navdrawer-container promote-layer">
		<h4>Navigation</h4>
		<ul>
			<li><a href="./index.html">Go to the first page!</a></li>
		</ul>
	</nav>

	<main>
	<section class="styleguide__feature-spotlight">
		<div class="featured-spotlight">
			<div class="container-medium">
				<div class="featured-spotlight__container g--pull-half">
					<div class="featured-spotlight__img">
						<img src="images/icons/placeholder--wide.png"
							alt="wide image placeholder example">
					</div>
					<div class="container-small">
						<h3 class="xxlarge">Personal info.</h3>
						<p>Here you will see some boring stuff like name and birth
							date.</p>
						<a href="#personal" class="cta--primary">Lets see.</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<a name="personal"></a>
	<div
		class="highlight-module  highlight-module--right   highlight-module--remember   highlight-module--inline ">
		<div class="highlight-module__container  icon-exclamation ">
			<div class="highlight-module__content   g-wide--pull-1  ">
				<p class="highlight-module__title">Contact and personal info</p>
				<ul class="highlight-module__list">
					<li>Name: ${cv_en_name}</li>
					<li>Surname: ${cv_en_surname}</li>
					<li>Birth: ${cv_en_birddate}</li>
					<li>Skype: ${cv_en_skype}</li>
					<li>Email: ${cv_en_email}</li>
					<li><a href="http://es.linkedin.com/in/mauriciomiralles" style="text-decoration:none;"><span style="font: 80% Arial,sans-serif; color:#0783B6;"><img src="https://static.licdn.com/scds/common/u/img/webpromo/btn_in_20x15.png" width="20" height="15" alt="View Mauricio Miralles's LinkedIn profile" style="vertical-align:middle" border="0">View Mauricio Miralles's profile</span></a></li>
				</ul>
			</div>
		</div>
	</div>

	<section class="styleguide__feature-spotlight">
		<div class="featured-spotlight">
			<div class="container-medium">
				<div class="featured-spotlight__container g--pull-half">
					<div class="featured-spotlight__img">
						<img src="images/icons/placeholder--wide.png"
							alt="wide image placeholder example">
					</div>
					<div class="container-small">
						<h3 class="xxlarge">Degrees.</h3>
						<p>Here you will see some of the courses and degrees I had
							studied.</p>
						<a href="#degree" class="cta--primary">Lets see.</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<a name="degree"></a>
	<div
		class="highlight-module  highlight-module--right   highlight-module--remember   highlight-module--inline ">
		<div class="highlight-module__container  icon-exclamation ">
			<div class="highlight-module__content   g-wide--pull-1  ">
				<p class="highlight-module__title">Degrees</p>
				<ul class="highlight-module__list">
					<li>M101J: MONGODB FOR JAVA DEVELOPERS - <a
						href="${cv_en_mongourl}"><img src="${cv_en_mongoimg}"
							alt="mongoImg"></a></li>
				</ul>
				<ul class="highlight-module__list">
					<li>${cv_en_college}</li>
				</ul>
			</div>
		</div>
	</div>

	<section class="styleguide__feature-spotlight">
		<div class="featured-spotlight">
			<div class="container-medium">
				<div class="featured-spotlight__container g--pull-half">
					<div class="featured-spotlight__img">
						<img src="images/icons/placeholder--wide.png"
							alt="wide image placeholder example">
					</div>
					<div class="container-small">
						<h3 class="xxlarge">Experience</h3>
						<p>Here you will see some of my last proyects and companies.</p>
						<a href="#experience" class="cta--primary">Lets see.</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<a name="experience"></a> <c:forEach items="${cv_en_companies}"
		var="companie" varStatus="status">
		<div
			class="highlight-module  highlight-module--right ${((status.index % 2) == 0) ? 'highlight-module--remember' : 'highlight-module--learning'} highlight-module--inline ">
			<div
				class="highlight-module__container  ${((status.index % 2) == 0) ? 'icon-exclamation' : 'icon-star'} ">
				<div class="highlight-module__content   g-wide--pull-1  ">
					<p class="highlight-module__title">
						<c:out value="${companie.name}" />
					</p>
					<ul class="highlight-module__list">
						<li>Client: <c:out value="${companie.client}" /></li>
						<li>When: <c:out value="${companie.duration}" /></li>
						<li>Role: <c:out value="${companie.role}" /></li>
						<li>Tecnologies: <c:out value="${companie.tecnologies}" /></li>
						<li>Curiosities: <c:out value="${companie.cusiosities}" /></li>
					</ul>
				</div>
			</div>
		</div>
	</c:forEach>
	
	<section class="styleguide__feature-spotlight">
		<div class="featured-spotlight">
			<div class="container-medium">
				<div class="featured-spotlight__container g--pull-half">
					<div class="featured-spotlight__img">
						<img src="images/icons/placeholder--wide.png"
							alt="wide image placeholder example">
					</div>
					<div class="container-small">
						<h3 class="xxlarge">Languages</h3>
						<p>Here you can see how many languages I speak and how clever.</p>
						<a href="#language" class="cta--primary">Lets see.</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<a name="language"></a> <c:forEach items="${cv_en_languages}"
		var="language" varStatus="status">
		<div
			class="highlight-module  highlight-module--right ${((status.index % 2) == 0) ? 'highlight-module--remember' : 'highlight-module--learning'} highlight-module--inline ">
			<div
				class="highlight-module__container  ${((status.index % 2) == 0) ? 'icon-exclamation' : 'icon-star'} ">
				<div class="highlight-module__content   g-wide--pull-1  ">
					<p class="highlight-module__title">
						<c:out value="${language.title}" />
					</p>
					<ul class="highlight-module__list">
						<li>How I speak in that language: <c:out value="${language.level}" /></li>
					</ul>
				</div>
			</div>
		</div>
	</c:forEach>

	<div
		class="highlight-module  highlight-module--left   highlight-module--learning  ">
		<div class="highlight-module__container  icon-star ">
			<div
				class="highlight-module__content   g-wide--push-1 g-wide--pull-1  g-medium--push-1   ">
				<p class="highlight-module__title">Off the record</p>
				<p class="highlight-module__text">This is just a beta version,
					at this moment I'm working to get this site Done!. Be patient in a
					few days that idea will be working</p>
			</div>
		</div>
	</div>
	</main>
	<!-- build:js scripts/main.min.js -->
	<script src="scripts/main.js"></script>
	<!-- endbuild -->

	<!-- Google Analytics: change UA-56423058-1 to be your site's ID -->
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o), m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script',
				'//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-56423058-1', 'auto');
		ga('send', 'pageview');
	</script>
	<!-- Built with love using Web Starter Kit -->
</body>
</html>